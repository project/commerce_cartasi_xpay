--------------------------------------------------------------------------------
Commerce CartaSi X-Pay
--------------------------------------------------------------------------------

This project integrates CartaSi - X-Pay 
(http://www.cartasi.it/gtwpages/common/index.jsp?id=OiRGdkfJWU) into the 
Drupal Commerce (http://drupal.org/project/commerce) payment and checkout 
systems. 
It currently supports off-site payment (Front Office).

Project mantainer: http://drupal.org/user/23887

Project homepage: http://drupal.org/sandbox/bessone/1878046

Issues: http://drupal.org/project/issues/1878046

INSTALLATION
--------------

* Install as usual, see 
 http://drupal.org/documentation/install/modules-themes/modules-7 
 for further information.

* Go to Store -> Configure -> Payment methods and press "enable" on 
 the disabled "CartaSi X-Pay" to enable the payment method

* To modify configuration options go to Store -> Configure -> Payment methods
 and press edit on "Commerce CartaSi XPay" row. 
 Scroll down to "Actions" and press "edit"

CREDITS
---------

Cards Icons designed by Phil Mathews from www.thewebdesignblog.co.uk
Additional Icons made by Marco Gandi (http://drupal.org/user/898386)

This Module is sponsored by 3ding Consulting (http://www.3dingconsulting.com)
